﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace Frames.Tests
{
    public class BaseTest : IDisposable
    {
        public IWebDriver Driver { get; }
        public BaseTest()
        {
            Driver = new ChromeDriver($@"{Environment.CurrentDirectory}\Resources");
            Driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 30);
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("http://jsbin.com/?html,output");
        }
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}