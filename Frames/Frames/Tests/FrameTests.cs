using Frames.Pages;
using Xunit;

namespace Frames.Tests
{
    public class FrameTest : BaseTest
    {
        private MainPage MainPage;
        private JSRunnerIFramePage JSRunnerIFramePage;
        private SandboxFrame SandboxFrame;
        public FrameTest()
        {
            MainPage = new MainPage(Driver);
        }
        [Fact]
        public void FrameSwitchingTest()
        {
            MainPage.SendKeysToActiveInput();
            JSRunnerIFramePage = MainPage.ClickOnFrame();
            Driver.SwitchTo().Frame(MainPage.Frame);
            SandboxFrame = JSRunnerIFramePage.ClickOnFrame();
            Driver.SwitchTo().Frame(JSRunnerIFramePage.Frame);
            SandboxFrame.SendKeysToInput();
        }
    }
}

