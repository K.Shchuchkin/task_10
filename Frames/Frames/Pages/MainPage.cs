﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Frames.Pages
{
    public class MainPage : BasePage
    {
        private Actions Actions;
        public MainPage(IWebDriver webDriver) : base(webDriver)
        {
            Actions = new Actions(Driver);
        }
        #region Web Elements
        public IWebElement Frame => Driver.FindElement(By.XPath("//iframe[@class='stretch']"));
        public IWebElement ActiveInput => Driver.FindElement(By.XPath("//div[@class='CodeMirror-activeline']"));

        #endregion
        #region Actions
        public void SendKeysToActiveInput() => Actions.Click(ActiveInput).SendKeys("<input id=\"test\"/>").Perform();
        public JSRunnerIFramePage ClickOnFrame()
        {
            Frame.Click();
            return new JSRunnerIFramePage(Driver);
        }
        #endregion
    }
}
