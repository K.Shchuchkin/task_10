﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Frames.Pages
{
    public class SandboxFrame : BasePage
    {
        private Actions Actions;
        public SandboxFrame(IWebDriver webDriver) : base(webDriver)
        {
            Actions = new Actions(Driver);
        }
        #region Web Elements
        public IWebElement Input => Driver.FindElement(By.Id("test"));

        #endregion
        #region Actions
        public void SendKeysToInput()
        {
            Actions.Click(Input).SendKeys("Success!").Perform();
        }
        #endregion
    }
}
