﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Frames.Pages
{
    public class JSRunnerIFramePage : BasePage
    {
        private Actions Actions;
        public JSRunnerIFramePage(IWebDriver webDriver) : base(webDriver)
        {
            Actions = new Actions(Driver);
        }
        #region Web Elements
        public IWebElement Frame => Driver.FindElement(By.XPath("//iframe[@name='JS Bin Output ']"));
        #endregion
        #region Actions
        public SandboxFrame ClickOnFrame()
        {
            Actions.Click(Frame).Perform();
            return new SandboxFrame(Driver);
        }
        #endregion
    }
}
