﻿using OpenQA.Selenium;

namespace Frames.Pages
{
    public class BasePage
    {
        protected IWebDriver Driver;
        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
