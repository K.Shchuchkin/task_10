﻿using OpenQA.Selenium;

namespace Alerts.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver Driver;
        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
