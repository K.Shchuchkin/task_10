using Alerts.Pages;
using Xunit;

namespace Alerts.Tests
{
    public class AlertTests : BaseTest
    {
        private AlertsPage AlertPage;
        private const string SendText = "Great site!";
        public AlertTests()
        {
            AlertPage = new AlertsPage(Driver);
        }
        [Fact]
        public void SimpleAlertTest()
        {
            AlertPage.ClickOnSimpleAlert();
            LogFile.WriteLine(Driver.SwitchTo().Alert().Text);
            Driver.SwitchTo().Alert().Accept();
        }
        [Fact]
        public void ConfirmPopUpTest()
        {
            AlertPage.ClickOnPopUpAlert();
            LogFile.WriteLine(Driver.SwitchTo().Alert().Text);
            Driver.SwitchTo().Alert().Accept();
        }
        [Fact]
        public void DismissPopUpTest()
        {
            AlertPage.ClickOnPopUpAlert();
            Driver.SwitchTo().Alert().Dismiss();
        }
        [Fact]
        public void PromptPopUpTest()
        {
            if (AlertPage.CheckIfCookieBarIsDisplayed())
            {
                AlertPage.CloseCookieBar();
            }
            AlertPage.ClickOnPromptAlert();
            LogFile.WriteLine(Driver.SwitchTo().Alert().Text);
            Driver.SwitchTo().Alert().SendKeys(SendText);
            Driver.SwitchTo().Alert().Accept();
        }
    }
}

