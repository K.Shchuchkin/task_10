﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;

namespace Alerts.Tests
{
    public abstract class BaseTest : IDisposable
    {
        protected IWebDriver Driver { get; private set; }

        protected readonly StreamWriter LogFile;
        public BaseTest()
        {
            Driver = new ChromeDriver($@"{Environment.CurrentDirectory}\Resources");
            Driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 30);
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/");
            LogFile = File.AppendText("logFile.txt");
        }
        public void Dispose()
        {
            LogFile.Close();
            Driver.Quit();
        }
    }
}