﻿using OpenQA.Selenium.Chrome;
using System;

namespace Waits.Tests
{
    public class BaseTest : IDisposable
    {
        public ChromeDriver Driver { get; }
        public BaseTest()
        {
            Driver = new ChromeDriver($@"{Environment.CurrentDirectory}\Resources");
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://catalog.onliner.by/tv");
        }
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}