﻿using Waits.Pages;
using Xunit;

namespace Waits.Tests
{
    public class TVComparison : BaseTest
    {
        private TVpage TVPage;
        private ComparisonPage ComparisonPage;
        public TVComparison()
        {
            TVPage = new TVpage(Driver);
        }
        [Fact]
        public void TVComparisonPageTest()
        {
            for(int i = 0; i < 2 && i < TVPage.GetNumberOfAvailableElements(); i++)
            {
                TVPage.SelectElementToCompareList(i);
            }
            ComparisonPage = TVPage.ClickOnComparisonPageButton();
            ComparisonPage.MoveToScreenDiagonalSection();
            ComparisonPage.ClickOnTipWindow();
            Assert.True(ComparisonPage.CheckIfTipWindowIsDisplayed());
            ComparisonPage.ClickOnTipWindow();
            Assert.False(ComparisonPage.CheckIfTipWindowIsDisplayed());
            ComparisonPage.RemoveFirstTVFromComparisonList();
        }
    }
}

