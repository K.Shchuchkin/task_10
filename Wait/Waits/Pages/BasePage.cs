﻿using OpenQA.Selenium;

namespace Waits.Pages
{
    public class BasePage
    {
        protected IWebDriver Driver;
        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
