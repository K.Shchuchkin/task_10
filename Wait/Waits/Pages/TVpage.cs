﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System;
using OpenQA.Selenium.Support.UI;

namespace Waits.Pages
{
    public class TVpage : BasePage
    {
        private WebDriverWait Wait;
        public TVpage(IWebDriver webDriver) : base(webDriver)
        {
            Wait = new WebDriverWait(Driver, new TimeSpan(0, 1, 0));
        }
        #region WebElements
        public ReadOnlyCollection<IWebElement> ToCompareListButton => Wait.Until(e => e.FindElements(By.XPath("//div[@class='schema-product__compare' and parent::div[parent::div[not(contains(@class, 'children'))]]]")));
        public IWebElement ComparisonPageButton => Wait.Until(e => e.FindElement(By.XPath("//div[@class='compare-button__state compare-button__state_initial']")));
        #endregion
        #region Actions
        public int GetNumberOfAvailableElements() => ToCompareListButton.Count;
        public void SelectElementToCompareList(int i) => ToCompareListButton[i].Click();
        public ComparisonPage ClickOnComparisonPageButton()
        {
            ComparisonPageButton.Click();
            return new ComparisonPage(Driver);
        }

        #endregion
    }
}
