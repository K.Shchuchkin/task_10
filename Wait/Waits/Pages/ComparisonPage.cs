﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace Waits.Pages
{
    public class ComparisonPage: BasePage
    {
        private WebDriverWait Wait;
        private Actions Actions;
        public ComparisonPage(IWebDriver webDriver) : base(webDriver)
        {
            Wait = new WebDriverWait(Driver, new TimeSpan(0, 1, 0));
            Actions = new Actions(Driver);
        }
        #region WebElements
        public IWebElement ScreenDiagonal => Wait.Until(e => e.FindElement(By.XPath("//span[text()='Диагональ экрана' and parent::td[not(contains(@class, 'duplicate'))]]")));
        public IWebElement HelpButton => Wait.Until(e => e.FindElement(By.XPath("//span[@data-tip-term='Диагональ экрана']")));
        public IWebElement TipWindow => Wait.Until(e => e.FindElement(By.XPath("//div[descendant::span[text()='Диагональ экрана'] and @id='productTableTip']")));
        public ReadOnlyCollection<IWebElement> DeleteElement => Wait.Until(e => e.FindElements(By.XPath("//a[@title='Удалить' and ancestor::table[@id='product-table']]")));
        #endregion
        #region Actions
        public void MoveToScreenDiagonalSection()
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", ScreenDiagonal);
            Actions.MoveToElement(ScreenDiagonal).Perform();
        }
        public void ClickOnTipWindow()
        {
            HelpButton.Click();
        }
        public bool CheckIfTipWindowIsDisplayed() => TipWindow.Displayed;
        public void RemoveFirstTVFromComparisonList() => DeleteElement[0].Click();
        #endregion
    }
}
