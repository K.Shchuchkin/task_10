﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;

namespace MultiWindow.Tests
{
    public class BaseTest : IDisposable
    {
        protected IWebDriver Driver { get; private set; }
        protected StreamWriter LogFile { get; private set; }
        public BaseTest()
        {
            Driver = new ChromeDriver($@"{Environment.CurrentDirectory}\Resources");
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://catalog.onliner.by/tv");
            Driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 30);
            LogFile = File.AppendText("logFile.txt");
        }
        public void Dispose()
        {
            LogFile.Close();
            Driver.Quit();
        }
    }
}