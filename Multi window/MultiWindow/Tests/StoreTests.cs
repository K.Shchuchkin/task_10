﻿using MultiWindow.Pages;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using Xunit;
using System.Diagnostics;

namespace MultiWindow.Tests
{
    public class Tests : BaseTest
    {
        private AppStorePage AppStorePage;
        private PlayStorePage PlayStorePage;
        private TVpage TVpage;
        private const string ExpectedName = "Каталог Onliner";
        public Tests()
        {
            TVpage = new TVpage(Driver);
        }
        [Fact]
        public void MultiWindowTest()
        {
            AppStorePage = TVpage.ClickOnAppStoreButton();
            PlayStorePage = TVpage.ClickOnPlayStoreButton();
            Assert.True(Driver.WindowHandles.Count == 3);
            string onlinerTVPage = Driver.WindowHandles[0];
            string googlePlay = Driver.WindowHandles[1];
            string appStore = Driver.WindowHandles[2];
            Driver.SwitchTo().Window(googlePlay);
            Assert.Contains(ExpectedName, PlayStorePage.GetAppNameText());
            PlayStorePage.ClickOnMoreButton();
            //To Log
            ReadOnlyCollection<IWebElement> similarApps = Driver.FindElements(By.XPath("//div[@class='ZmHEEd ']/*"));
            var watch = new Stopwatch();
            watch.Start();
            while (true)
            {
                try
                {
                    similarApps = Driver.FindElements(By.XPath("//div[@class='ZmHEEd ']/*"));
                    if(similarApps.Count != 0)
                    {
                        break;
                    }
                }
                catch{ }
                if(watch.Elapsed.TotalSeconds >= 5)
                {
                    watch.Stop();
                    break;
                }
            }
            LogFile.WriteLine(similarApps.Count);
            Driver.SwitchTo().Window(appStore);
            AppStorePage.ClickOnMoreButton();
            Driver.Close();
            Driver.SwitchTo().Window(onlinerTVPage);
            TVpage.ClickOnAdvBanner();
        }
    }
}

