﻿using OpenQA.Selenium;

namespace MultiWindow.Pages
{
    public class BasePage
    {
        protected IWebDriver Driver;
        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
