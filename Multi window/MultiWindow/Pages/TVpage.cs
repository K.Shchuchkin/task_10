﻿using OpenQA.Selenium;

namespace MultiWindow.Pages
{
    public class TVpage : BasePage
    {
        public TVpage(IWebDriver webDriver) : base(webDriver)
        {

        }
        #region WebElements
        public IWebElement AppStoreButton => Driver.FindElement(By.XPath("//a[contains(@href, 'itunes')]"));
        public IWebElement GooglePlayButton => Driver.FindElement(By.XPath("//a[contains(@href, 'google') and parent::div[contains(@class, 'filter')]]"));
        public IWebElement AdvFrame => Driver.FindElement(By.XPath("//iframe[ancestor::div[contains(@id, 'adfox_157650090989294157')]]"));
        #endregion
        #region Actions
        public AppStorePage ClickOnAppStoreButton()
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", AppStoreButton);
            AppStoreButton.Click();
            return new AppStorePage(Driver);
        }
        public PlayStorePage ClickOnPlayStoreButton()
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", GooglePlayButton);
            GooglePlayButton.Click();
            return new PlayStorePage(Driver);
        } 
        public void ClickOnAdvBanner() => AdvFrame.Click();
        #endregion
    }
}
