﻿using OpenQA.Selenium;

namespace MultiWindow.Pages
{
    public class AppStorePage: BasePage
    {
        public AppStorePage(IWebDriver webDriver) : base(webDriver)
        {

        }
        #region WebElements
        public IWebElement AppName => Driver.FindElement(By.XPath("//h1[parent::header and contains(@class, 'title')]"));
        public IWebElement MoreButton => Driver.FindElement(By.XPath("//button[ancestor::div[@class='section__description']]"));
        #endregion
        #region Actions
        public void ClickOnMoreButton() 
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView({block: \"center\"})", MoreButton);
            MoreButton.Click();
        } 
        #endregion
    }
}
