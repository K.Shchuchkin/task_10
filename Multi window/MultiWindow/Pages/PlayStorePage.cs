﻿using OpenQA.Selenium;

namespace MultiWindow.Pages
{
    public class PlayStorePage : BasePage
    {
        public PlayStorePage(IWebDriver webDriver) : base(webDriver)
        {
        }
        #region WebElements
        public IWebElement AppName => Driver.FindElement(By.XPath("//span[parent::h1]"));
        public IWebElement MoreButton => Driver.FindElement(By.XPath("//a[parent::div[@class='W9yFB']]"));
        #endregion
        #region Actions
        public string GetAppNameText() => AppName.Text;
        public void ClickOnMoreButton() => MoreButton.Click();
        #endregion
    }
}
